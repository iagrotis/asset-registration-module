<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Library of interface functions and constants.
 *
 * @package     contentsimulator
 * @copyright   2019 Your Name <you@example.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Return if the plugin supports $feature.
 *
 * @param string $feature Constant representing the feature.
 * @return true | null True if the feature is supported, null otherwise.
 */
function contentsimulator_supports($feature) {
    switch ($feature) {
        case FEATURE_MOD_INTRO:
            return true;
        default:
            return null;
    }
}

/**
 * Saves a new instance of the contentsimulator into the database.
 *
 * Given an object containing all the necessary data, (defined by the form
 * in mod_form.php) this function will create a new instance and return the id
 * number of the instance.
 *
 * @param object $moduleinstance An object from the form.
 * @param mod_contentsimulator_mod_form $mform The form.
 * @return int The id of the newly inserted record.
 */
function contentsimulator_add_instance($moduleinstance, $mform = null) {
    global $DB, $USER;

    $moduleinstance->timecreated = time();

//Register content to Launchserver url 

$userinstanse = $DB->get_record('user', array('id' => $USER->id), '*', MUST_EXIST);
$username = $userinstanse->username;
$useremail = $userinstanse->email;

$thename = $moduleinstance->name;
$owner = new stdClass();
$owner->email=$useremail;
$owner->name=$username;

$obj = new stdClass();
$obj->title= $thename;
$obj->url=$_SESSION["task_url"];
$obj->owner=$owner;

// Read JSON file
$json = file_get_contents('config.json');

//Decode JSON
$json_data = json_decode($json,true);

//$url = $json_data['LS_CONTENT_REGISTER'];
$url = "https://easier-launch.cyric.io/content/register";

$payload = json_encode($obj);

// Prepare new cURL resource
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLINFO_HEADER_OUT, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

// Set HTTP Header for POST request 
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Content-Type: application/json',
    'Access-Control-Allow-Origin: *',
    'Access-Control-Allow-Methods: *',
    'Content-Length: ' . strlen($payload))
);

// Submit the POST request
$result = curl_exec($ch);

$json = json_decode($result, true);
// Close cURL session handle
curl_close($ch);


$registeredassetinsance->url=$_SESSION["task_url"];
$registeredassetinsance->contentkey=$json['ContentKey'];

$contid = $DB->insert_record('registeredcontent', $registeredassetinsance);

$moduleinstance->contentid = $contid;
//===============================================================================================================

$id = $DB->insert_record('contentsimulator', $moduleinstance);

return $id;

}

/**
 * Updates an instance of the contentsimulator in the database.
 *
 * Given an object containing all the necessary data (defined in mod_form.php),
 * this function will update an existing instance with new data.
 *
 * @param object $moduleinstance An object from the form in mod_form.php.
 * @param mod_contentsimulator_mod_form $mform The form.
 * @return bool True if successful, false otherwise.
 */
function contentsimulator_update_instance($moduleinstance, $mform = null) {
    global $DB;

    $moduleinstance->timemodified = time();
    $moduleinstance->id = $moduleinstance->instance;

    return $DB->update_record('contentsimulator', $moduleinstance);
}

/**
 * Removes an instance of the contentsimulator from the database.
 *
 * @param int $id Id of the module instance.
 * @return bool True if successful, false on failure.
 */
function contentsimulator_delete_instance($id) {
    global $DB;

    $exists = $DB->get_record('contentsimulator', array('id' => $id));
    if (!$exists) {
        return false;
    }

    $DB->delete_records('contentsimulator', array('id' => $id));

    return true;
}
