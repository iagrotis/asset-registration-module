<?php

require(__DIR__.'/../../config.php');
require_once(__DIR__.'/lib.php');

function httpPost($url, $useremail)
{
    $obj = new stdClass();
    $obj->email=$useremail;
    $payload = json_encode($obj);
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL,$url); 
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch,CURLINFO_HEADER_OUT, true);
    curl_setopt($ch,CURLOPT_POST, true);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $payload);
    // Set HTTP Header for POST request 
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Access-Control-Allow-Origin: *',
        'Access-Control-Allow-Methods: *',
        'Content-Length: ' . strlen($payload))
    );

    // Submit the POST request
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
};

function httpGet($url)
{
    $ch = curl_init(); 
    curl_setopt($ch,CURLOPT_URL,$url);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    $output=curl_exec($ch); 
    curl_close($ch);
    return $output;
};


$id = htmlspecialchars($_GET["id"]);

global $DB;
$assetinstanse = $DB->get_record('block_assetregister', array('id' => $id), '*', MUST_EXIST);
$link = $assetinstanse->url;

$userinstanse = $DB->get_record('user', array('id' => $USER->id), '*', MUST_EXIST);
$useremail = $userinstanse->email;

/**
 * This should change if other Assets require authentication using Username and Password.
 * Currently only works for AMELIE
 */
if(isset($_GET['Password'])&&isset($_GET['Username'])){
	$link = $assetinstanse->url.'?Username='.htmlspecialchars($_GET['Username']).'&Password='.htmlspecialchars($_GET['Password']);
}
else{
	$link = $assetinstanse->url;
};

echo httpPost($link, $useremail);
//echo httpGet($link);

?>