<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The main contentsimulator configuration form.
 *
 * @package     contentsimulator
 * @copyright   2019 Your Name <you@example.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/moodleform_mod.php');

/**
 * Module instance settings form.
 *
 * @package    contentsimulator
 * @copyright  2019 Your Name <you@example.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class mod_contentsimulator_mod_form extends moodleform_mod {


    /**
     * Defines forms elements
     */
    public function definition() {
        global $CFG, $PAGE;

        $PAGE->requires->js('/mod/contentsimulator/launchserver.js','*');

        $mform = $this->_form;

        // Adding the "general" fieldset, where all the common settings are showed.
        $mform->addElement('header', 'general', get_string('general', 'form'));

        // Adding the standard "name" field.
        $mform->addElement('text', 'name', get_string('contentsimulatorname', 'contentsimulator'), array('size' => '64'));

        if (!empty($CFG->formatstringstriptags)) {
            $mform->setType('name', PARAM_TEXT);
        } else {
            $mform->setType('name', PARAM_CLEANHTML);
        }

        $mform->addRule('name', null, 'required', null, 'client');
        $mform->addRule('name', get_string('maximumchars', '', 255), 'maxlength', 255, 'client');
        $mform->addHelpButton('name', 'contentsimulatorname', 'contentsimulator');

        echo  "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js'></script>";
		echo  "<script src='http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js'></script>";

        global $DB;
        $amelie = $DB->get_record_sql("select * from mdl_block_assetregister where name='AMELIE'");
        $options = $DB->get_records_menu('block_assetregister', $conditions=null, $sort='', $fields='*', $limitfrom=0, $limitnum=0);
        $select = $mform->addElement('select', 'assetid', 'Select External Asset', $options);

		$text = $mform->addElement('text', 'AMELIEusername', 'Insert your AMELIE username:' );
		$pass = $mform->addElement('password', 'AMELIEpassword','Insert your AMELIE password:');
		$submit = $mform->addElement('button', 'AMELIEsubmit','Enter your AMELIE credentials and click here');
		$mform->hideIf('AMELIEusername', 'assetid', 'neq', $amelie->id);
		$mform->hideIf('AMELIEpassword', 'assetid', 'neq', $amelie->id);
		$mform->hideIf('AMELIEsubmit', 'assetid', 'neq', $amelie->id);

        // Create an empty select element.
        $select = $mform->addElement('select', 'tasks_list', 'Select Activity');
        $mform->addRule('tasks_list', null, 'required');

        // Adding the standard "intro" and "introformat" fields.
        if ($CFG->branch >= 29) {
            $this->standard_intro_elements();
        } else {
            $this->add_intro_editor();
        }

        // Add standard elements.
        $this->standard_coursemodule_elements();

        // Add standard buttons.
        $this->add_action_buttons();

    }

}

