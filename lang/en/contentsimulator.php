<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Plugin strings are defined here.
 *
 * @package     contentsimulator
 * @category    string
 * @copyright   2019 Your Name <you@example.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['contentsimulator:addcoursetool'] = 'Add course-specific tool configurations';
$string['contentsimulator:addinstance'] = 'nAdd new Simulator content tool activities';
$string['contentsimulator:addinstance'] = 'Add instance';
$string['contentsimulatorfieldset'] = 'Custom example fieldset.';
$string['contentsimulatorname'] = 'External Asset Activity';
$string['contentsimulatorname_help'] = 'This is the content of the help tooltip associated with the newmodulename field';

$string['contentsimulatorurl'] = 'External Asset Url';
$string['contentsimulatorurl_help'] = 'This is the url for the external asset';

$string['contentsimulatorsettings'] = 'Settings';
$string['missingidandcmid'] = 'Missing id and cmid';
$string['modulename'] = 'External Asset';
$string['modulename_help'] = 'Use the simulator content module for adding external assets';
$string['modulenameplural'] = 'External Asset Module';
$string['nonewmodules'] = 'No newmodules';
$string['pluginadministration'] = 'External Asset administration';
$string['pluginname'] = 'External Asset plugin';
$string['pluginname'] = 'External Asset plugin';
$string['view'] = 'View';




